.. IceBoard User Manual documentation master file, created by
   sphinx-quickstart on Fri Jan 23 10:16:27 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
====================
IceBoard User Manual
====================

.. toctree::
   :maxdepth: 2
   :numbered:

.. Indices and tables

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

.. include:: Iceboard_User_Manual.rst

